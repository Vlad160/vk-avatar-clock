const express = require('express');
const bodyParser = require('body-parser')
const vkClient = require('./client.js');
const app = express();

const CLIENTS = [];

const PORT = process.env.PORT || 3000;

app.use(express.static('public'));
const urlencodedParser = bodyParser.urlencoded({ extended: true })


app.post('/add', urlencodedParser, (req, res) => {
    const { body } = req;
    if (!body) {
        return res.sendStatus(400);
    }
    if (!body['user_id'] || !body['access_token']) {
        return res.sendStatus(400);
    }

    CLIENTS.push(new vkClient(body['access_token'], body['user_id']));
    res.send('Добавил тебя');

});

app.get('/start', (req, res) => {
    CLIENTS.forEach(x => x.start());
    res.send(`Все клиенты стартанули! ${CLIENTS.length}`);
});

app.get('/stop', (req, res) => {
    CLIENTS.forEach(x => x.stop());
    res.send(`Все клиенты остановились! ${CLIENTS.length}`);

});

app.get('/trigger', (req, res) => {
    CLIENTS.forEach(x => x.trigger());
    res.send(`Все клиенты тригернулись! ${CLIENTS.length}`);

});

app.listen(PORT, () => {
    console.log(`Server is running on ${PORT}`);
});