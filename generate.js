const { convert } = require('convert-svg-to-png');
const svgson = require('svgson');
const fs = require('fs');

const STROKE_WIDTH = 'stroke-width';
const FILL = 'fill';
const TOP = 'top';
const MID = 'mid';
const BOT = 'bot';
const TOP_RIGHT = 'top_r';
const TOP_LEFT = 'top_l';
const BOT_LEFT = 'bot_l';
const BOT_RIGHT = 'bot_r';

const numberMap = {
    0: zero,
    1: one,
    2: two,
    3: three,
    4: four,
    5: five,
    6: six,
    7: seven,
    8: eight,
    9: nine
}


function zero(node) {
    updateGroup(node, [TOP, BOT, TOP_LEFT, TOP_RIGHT, BOT_LEFT, BOT_RIGHT]);
}

function one(node) {
    updateGroup(node, [TOP_RIGHT, BOT_RIGHT]);
}

function two(node) {
    updateGroup(node, [TOP, TOP_RIGHT, MID, BOT_LEFT, BOT]);
}

function three(node) {
    updateGroup(node, [TOP, TOP_RIGHT, BOT_RIGHT, BOT, MID]);
}

function four(node) {
    updateGroup(node, [TOP_LEFT, MID, TOP_RIGHT, BOT_RIGHT]);
}

function five(node) {
    updateGroup(node, [TOP, TOP_LEFT, MID, BOT_RIGHT, BOT]);
}

function six(node) {
    updateGroup(node, [TOP_LEFT, MID, BOT_RIGHT, BOT, BOT_LEFT]);
}

function seven(node) {
    updateGroup(node, [TOP_RIGHT, BOT_RIGHT, TOP]);
}

function eight(node) {
    updateGroup(node, [TOP, BOT, MID, TOP_LEFT, TOP_RIGHT, BOT_LEFT, BOT_RIGHT]);
}

function nine(node) {
    updateGroup(node, [TOP, BOT, MID, TOP_LEFT, TOP_RIGHT, BOT_RIGHT]);
}

function getElementById(nodes, id) {
    const group = nodes.find(x => x.attributes.id === id);
    return group;
}

function activate(node) {
    node.attributes[STROKE_WIDTH] = '1.5';
    node.attributes[FILL] = '#ff0000';
}

function deactivate(node) {
    node.attributes[STROKE_WIDTH] = '0';
    node.attributes[FILL] = 'none';
}

function updateGroup(group, toActivate) {
    group.children.forEach(node => {
        const { id } = node.attributes;
        toActivate.includes(id) ? activate(node) : deactivate(node);
    });
}

async function generatePhoto() {
    const jsonSVG = await svgson.parse(fs.readFileSync('./template.svg', { encoding: 'utf-8' }));
    const drawRoot = jsonSVG.children[1].children;
    const date = new Date();
    const hours = `0${date.getHours()}`.substr(-2);
    const minutes = `0${date.getMinutes()}`.substr(-2);
    const time = hours + minutes;
    [1, 2, 3, 4].forEach((x, i) => {
        const node = getElementById(drawRoot, `num${x}`);
        const num = time[i];
        const fn = numberMap[+num];
        fn(node);
    });
    const svg = svgson.stringify(jsonSVG);
    const png = await convert(svg);
    // fs.writeFileSync('rofl.png', png);
    return png;
};

module.exports = generatePhoto;