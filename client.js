const MINUTE = 60e3;
const generatePhoto = require('./generate');
const vk = require('vk-io');
const { VK } = vk;

module.exports = class VkClient {
    constructor(token, user_id) {
        this.token = token;
        this.user_id = user_id;
        this.vk = new VK({ token });
        this.intervalId = null;
    }

    get isRunning() {
        return !!this.intervalId;
    }
    async removePhoto() {
        const user = await this.vk.api.users.get({
            user_ids: this.user_id,
            fields: ['photo_id']
        });
        const photoId = user[0].photo_id.split('_')[1];
        return this.vk.api.photos.delete({
            photo_id: +photoId
        });
    }

    async setPhoto(img) {
        return this.vk.upload.ownerPhoto({
            source: img,
            timeout: MINUTE,
            user_id: this.user_id
        });
    }

    async start() {
        if (this.isRunning) {
            return;
        }
        return setInterval(() => this.trigger(), MINUTE);
    }

    async stop() {
        if (this.isRunning) {
            clearInterval(this.intervalId);
        }
    }

    async trigger() {
        let img = null;
        try {
            console.log('Generating photo');
            img = await generatePhoto();
            console.log('Removing photo');
            await this.removePhoto();
        } catch (e) {
            console.error(e);
            return;
        }
        try {
            console.log('Setting new photo');
            await this.setPhoto(img);
        } catch (e) {
            console.error(e);
            this.stop();
        }
    }

}